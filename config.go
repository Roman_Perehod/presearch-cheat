package main

import (
	"bytes"
	"github.com/BurntSushi/toml"
	"log"
)

var config_instance *TomlConfig

type TomlConfig struct {
	Main MainConfig
}

type MainConfig struct {
	ExecutorUrl string
	Credentials string
}

func GetConfig() *TomlConfig {
	return config_instance
}

func NewConfig(file string) (*TomlConfig, error) {
	config_instance = &TomlConfig{}

	if _, err := toml.DecodeFile(file, config_instance); err != nil {
		return nil, err
	}

	dumpConfig(config_instance)

	// Main
	if len(config_instance.Main.ExecutorUrl) == 0 {
		log.Fatalln("Main.HostStr is required.")
	}

	return config_instance, nil
}

func dumpConfig(config *TomlConfig) {
	var buffer bytes.Buffer
	e := toml.NewEncoder(&buffer)
	err := e.Encode(config)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Sevice started with config:\n %s", buffer.String())
}
