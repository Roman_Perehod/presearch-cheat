FROM golang:latest
#ADD  /usr/share/dict/words /usr/share/dict
WORKDIR /app
ADD . /app
RUN cd /app && go-wrapper download && go build -o presearch-cheat && mkdir -p /usr/share/dict
ENTRYPOINT ./presearch-cheat