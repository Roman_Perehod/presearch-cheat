package main

import (
	"fmt"
	"sourcegraph.com/sourcegraph/go-selenium"
	"time"
	"flag"
	"log"
	"strings"
	"github.com/tjarratt/babble"
	"math/rand"
)

var (
	webDriver      selenium.WebDriver
	configFileName = flag.String("config", "config.toml", "Config file name OR ")

	loginUrl = "https://www.presearch.org/login"
	homeUrl  = "https://www.presearch.org/"
)

type Report struct {
	StartBalance string
	EndBalance   string
}

func init() {
	flag.Parse()
	// Red config from file
	_, err := NewConfig(*configFileName)
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	credentials := parseCSV()
	fmt.Printf("Credentials count: %d", len(credentials))
	initWedDriver()

	defer webDriver.Quit()

	report := make(map[string]Report, len(credentials))

	for email, pass := range credentials {
		logout()
		if err := login(strings.TrimSpace(email), strings.TrimSpace(pass)); err == nil {
			// Check balance
			bal, err := checkBalance()
			if err != nil {
				fmt.Println("Failed to get balabce. Check login script")
				continue
			}
			itr := random(33, 40)
			for i := 0; i < itr; i++ {
				goToHome()
				doSearch()
				goToHome()
				checkBalance()
			}
			endBal, _ := checkBalance()
			report[email] = Report{StartBalance: bal, EndBalance: endBal}
		} else {
			continue
		}
	}
	for k, v := range report {
		fmt.Printf("Progress by accout %s: start balance %s, end balance %s\n", k, v.StartBalance, v.EndBalance)
	}
}

func initWedDriver() {
	time.Sleep(time.Duration(3) * time.Second)
	var err error
	caps := selenium.Capabilities(map[string]interface{}{"browserName": "chrome"})
	if webDriver, err = selenium.NewRemote(caps, GetConfig().Main.ExecutorUrl); err != nil {
		panic(fmt.Sprintf("Failed to open session: %s\n", err))
	}
}

func login(login, password string) error {
	// Go to login page
	err := webDriver.Get(loginUrl)
	if err != nil {
		fmt.Printf("Failed to load page %s: %s\n", loginUrl, err)
		return err
	}

	fmt.Printf("Success to load page %s\n", loginUrl)
	time.Sleep(time.Duration(3) * time.Second)

	// Login
	fmt.Printf("Try %s\n", login)
	// Set email
	var elEmail selenium.WebElement
	elEmail, err = webDriver.FindElement(selenium.ById, "email")
	if err != nil {
		fmt.Printf("Failed to find element email: %s\n", err)
		return err
	}

	if err := elEmail.SendKeys(login); err == nil {
		fmt.Printf("Send login: %s\n", login)
	} else {
		fmt.Printf("Failed to get text of element login: %s\n", err)
		return err
	}

	// Set password
	var elPass selenium.WebElement
	elPass, err = webDriver.FindElement(selenium.ById, "password")
	if err != nil {
		fmt.Printf("Failed to find element password: %s\n", err)
		return err
	}

	if err := elPass.SendKeys(password); err == nil {
		fmt.Println("Send password: ******")
	} else {
		fmt.Printf("Failed to send password: %s\n", err)
		return err
	}

	// Click logon button
	var loginButton selenium.WebElement
	loginButton, err = webDriver.FindElement(selenium.ByXPATH, "//form[@method='POST']//button")
	if err != nil {
		fmt.Printf("Failed to find element login button: %s\n", err)
		return err
	}

	if err := loginButton.Click(); err != nil {
		fmt.Printf("Failed to click login button: %s\n", err)
		return err
	} else {
		fmt.Println("Success to click login button!")
	}

	time.Sleep(time.Duration(5) * time.Second)

	return err
}

func logout() {
	err := webDriver.Get(homeUrl)
	if err != nil {
		fmt.Printf("Failed to load page %s: %s\n", homeUrl, err)
		return
	}

	webDriver.ExecuteScript("document.getElementById('logout-form').submit();", nil)

	fmt.Println("Try to logout")
	time.Sleep(time.Duration(1) * time.Second)
}

func checkBalance() (string, error) {
	selector := ".balance"
	elem, err := webDriver.FindElement(selenium.ByCSSSelector, selector)
	if err != nil {
		fmt.Printf("Failed to find element %s: %s\n", selector, err)
		return "", err
	}

	text, err := elem.Text()
	if err != nil {
		fmt.Printf("Failed to get balabce: %s\n", err)
		return "", err
	}
	fmt.Printf("Balance: %s\n", text)
	return text, nil
}

func doSearch() {
	// Do search
	strSelector := "search"
	var elSearch selenium.WebElement
	elSearch, err := webDriver.FindElement(selenium.ById, strSelector)
	if err != nil {
		fmt.Printf("Failed to find element %s: %s\n", strSelector, err)
		return
	}

	babbler := babble.NewBabbler()
	babbler.Separator = " "
	babbler.Count = random(1, 5)
	searchVal := babbler.Babble()
	if err := elSearch.SendKeys(searchVal); err == nil {
		fmt.Printf("Send search str: %s\n", searchVal)
	} else {
		fmt.Printf("Failed to get text of element: %s\n", err)
		return
	}

	time.Sleep(time.Duration(2) * time.Second)

	// Search button click
	// webDriver.ExecuteScript("document.querySelector('#search-form button').click();", nil)
	// or
	btnSelector := "//form[@id='search-form']//button"
	searchButton, err := webDriver.FindElement(selenium.ByXPATH, btnSelector)
	if err != nil {
		fmt.Printf("Failed to find element %s: %s\n", btnSelector, err)
		return
	}

	err = searchButton.Click()
	if err != nil {
		fmt.Printf("Failed to click search button: %s\n", err)
		return
	} else {
		fmt.Println("Success to click to search button!")
	}

	time.Sleep(time.Duration(random(5, 15)) * time.Second)
}

func goToHome() {
	err := webDriver.Get(homeUrl)
	if err != nil {
		fmt.Printf("Failed to load page %s: %s\n", homeUrl, err)
		return
	}
	time.Sleep(time.Duration(3) * time.Second)
}

func random(min, max int) int {
	rand.Seed(time.Now().Unix())
	return rand.Intn(max-min) + min
}
