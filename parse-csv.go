package main

import (
	"os"
	"encoding/csv"
	"bufio"
	"io"
	"fmt"
)

func parseCSV() map[string]string {
	data := make(map[string]string)

	f, err := os.Open(GetConfig().Main.Credentials)
	if err != nil {
		panic(fmt.Sprintf("Can't open file %s", GetConfig().Main.Credentials))
	}

	r := csv.NewReader(bufio.NewReader(f))
	for {
		record, err := r.Read()
		// Stop at EOF.
		if err == io.EOF {
			break
		}

		if len(record) == 2 {
			data[record[0]] = record[1]
		}
	}

	return data
}
